
Pour reproduire les résultats présentés dans le rapport, il suffit de lancer les scripts Python "_evaluation.py" associés aux différentes approches. Si le repository git a été cloné, tous les paths seront bons et les scripts devraient tourner directement sans problème.
